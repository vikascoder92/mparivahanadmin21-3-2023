import { Component } from '@angular/core';
import { NavigationStart, Router } from '@angular/router';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'mpariwahan';
  showHead: boolean = true;

  constructor(private router: Router) {

      router.events.forEach((event) => {
        if (event instanceof NavigationStart) {
          if (event['url'] == '/' || event['url'] == '/contact' || event['url'] == '/login' || event['url'] == '/dashboard' || event['url'] == '/privacy' || event['url'] == '/terms' || event['url'] == '/privacy' || event['url'] == '/WebsitePolicy' || event['url'] == '/hyperlinkPolicy' || event['url'] == '/contentPolicy' || event['url'] == '/contingency') {
            this.showHead = true;
          } else {
            this.showHead = false;
          }
        }
      });
    }
}
