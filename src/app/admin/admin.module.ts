import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { ContentComponent } from './include/content/content.component';
import { DashboardComponent } from './adminComponent/dashboard/dashboard.component';
import { AdminHeaderComponent } from './include/admin-header/admin-header.component';
import { SidebarComponent } from './include/sidebar/sidebar.component';
import { AuthenticationService } from './service/authentication.service';
import { FormsModule } from '@angular/forms';
import { AdminComponent } from './admin.component';
import { HttpClientModule } from '@angular/common/http';
import { ProfileComponent } from './adminComponent/profile/profile.component';
import { AuthGuard } from './service/auth.guard';
import { ContactComponent } from './adminComponent/contact/contact.component';
import { UserDetailsComponent } from './adminComponent/user-details/user-details.component';
import { StatesComponent } from './adminComponent/states/states.component';
import { ErrorComponent } from './adminComponent/error/error.component';
import { FormComponent } from './adminComponent/form/form.component';
import { UpdateServiceComponent } from './adminComponent/update-service/update-service.component';
import { GetEnableServicesComponent } from './adminComponent/get-enable-services/get-enable-services.component';
import { ConfermationComponent } from './adminComponent/confermation/confermation.component';
import { GetCountService } from '../services/get-count.service';


@NgModule({
  declarations: [
    ContentComponent,
    DashboardComponent,
    AdminHeaderComponent,
    SidebarComponent,
    AdminComponent,
    ProfileComponent,
    ContactComponent,
    UserDetailsComponent,
    StatesComponent,
    ErrorComponent,
    FormComponent,
    UpdateServiceComponent,
    GetEnableServicesComponent,
    ConfermationComponent,



  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    FormsModule,
    HttpClientModule

  ],
  providers: [AuthenticationService, AuthGuard,GetCountService],

  bootstrap: [AdminComponent]
})
export class AdminModule { }
