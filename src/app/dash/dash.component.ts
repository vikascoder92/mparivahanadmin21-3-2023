import { Component, OnInit } from '@angular/core';
import { EnabledService } from '../admin/service/enabled-service.service';
import { GetCountService } from '../services/get-count.service';

@Component({
  selector: 'app-dash',
  templateUrl: './dash.component.html',
  styleUrls: ['./dash.component.css']
})
export class DashComponent implements OnInit {
  listData: any;
  SarathiList:any;
  filters = {
    "mparSarathiLog": {
        "sarlogStateCd": "ALL"
    }
};
  constructor(private api: GetCountService, private enabledService: EnabledService) { }
  IsSpinner = true
  ngOnInit(): void {
    this.getData()
    this.sarathiData()
    this.mparSarathiCount();
    setTimeout(() => {
      this.IsSpinner = false
      }, 2000);
  }
  ChallanCount:any
  SarathiCount:any
  getData() {
    let url = "https://staging.parivahan.gov.in/admin-stg/api/dashboard/get-echallan-count"
    let body =
    {
      "mparEchallanLog": {
        "echlogStateCd": "ALL"
      }
    }

    this.api.PostData(url, body).subscribe((d: any) => {
     // console.log(d)
      this.ChallanCount = d.challanCount
    })

  };



  sarathiData() {
    let sarathiUrl = "https://staging.parivahan.gov.in/admin-stg/api/dashboard/get-sarathi-count"
    let body =
    {
      "mparSarathiLog": {
          "sarlogStateCd": "ALL"
      }
  }
  
    this.api.PostData(sarathiUrl, body).subscribe((s: any) => {
      // console.log(s)
      this.SarathiCount = s.sarathiCount
    })


    
  }


  mparSarathiCount(): void {
    let sarathiUrl = "https://staging.parivahan.gov.in/admin-stg/api/dashboard/get-sarathi-count"
    this.api.PostData(sarathiUrl,this.filters).subscribe((a: any) => {
     console.log(a)
        this.SarathiList = a.sarathiCount;
      })
    }

}
