import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { single } from 'rxjs';
import { AdminModule } from './admin/admin.module';
import { ContactComponent } from './admin/adminComponent/contact/contact.component';
import { ContactDetailsComponent } from './contact-details/contact-details.component';
import { DashComponent } from './dash/dash.component';
import { ContentPolicyComponent } from './footerLink/content-policy/content-policy.component';
import { ContingencyComponent } from './footerLink/contingency/contingency.component';
import { HyperlinkPolicyComponent } from './footerLink/hyperlink-policy/hyperlink-policy.component';
import { PrivacyPolicyComponent } from './footerLink/privacy-policy/privacy-policy.component';
import { TermsComponent } from './footerLink/terms/terms.component';
import { WebsitePolicyComponent } from './footerLink/website-policy/website-policy.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'dashboard', component: DashComponent },
  { path: 'login', component: LoginComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'contact', component: ContactDetailsComponent },
  { path: 'privacy', component: PrivacyPolicyComponent },
  { path: 'terms', component: TermsComponent },
  { path: 'WebsitePolicy', component: WebsitePolicyComponent },
  { path: 'hyperlinkPolicy', component: HyperlinkPolicyComponent },
  { path: 'contentPolicy', component: ContentPolicyComponent },
  { path: 'contingency', component: ContingencyComponent },
  {
    path: 'admin',
    loadChildren: () =>
      import('./admin/admin.module').then((m) => m.AdminModule)
  },




]
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]

})
export class AppRoutingModule { }
