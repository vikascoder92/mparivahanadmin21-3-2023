import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthenticationService } from './admin/service/authentication.service';

@Injectable()
export class InterceptorInterceptor implements HttpInterceptor {
  constructor(private autthservice : AuthenticationService) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    const tokennizedReq = request.clone(
      {
        setHeaders :{ Authorization: `Bearer ${this.autthservice.getAuthToken()}`}
      }
    )
    return next.handle(tokennizedReq);
   
  }
}
