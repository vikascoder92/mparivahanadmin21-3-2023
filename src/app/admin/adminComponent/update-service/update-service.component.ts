import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GetCountService } from 'src/app/services/get-count.service';
import { EnabledService } from '../../service/enabled-service.service';

@Component({
  selector: 'app-update-service',
  templateUrl: './update-service.component.html',
  styleUrls: ['./update-service.component.css']
})
export class UpdateServiceComponent implements OnInit {
  Chalan:any
  list: any;
  stateCode: string;
  servMode: string;
  serviceId: number;

  constructor( private enabledService: EnabledService,public router:ActivatedRoute , private _router: Router) {
    const params = this.router.snapshot.params;
    this.stateCode = params['stateCode'];
    this.servMode = params['serviceMode'];
    this.serviceId = params['serviceId'];
    console.log(params);

  }
  IsSpinner = true


  ngOnInit(): void {
      
    this.geupdateenable()
    //alert(this.router.snapshot.params['servId'])
    
  }
  geupdateenable(): void {
    let body = {
      "mparEnabledService": {
        "servStateCd": this.stateCode,
        "servModule": this.servMode
      },
    }
   

    this.enabledService.getAllEnabledServices(body).subscribe((d: any) => {
      const res: any = d.filter( (ele:any) => ele.servId == this.serviceId );
      if(res.length > 0){
        this.list = res[0];
      }else{
        this._router.navigate(['/admin/getenableservice'])
      }
      
      console.log(this.list)

    })

  }


  updateData(value: any) {
    console.log("values", value, this.list);
    const payload: object = {
      "mparEnabledService": {
          "servStateCd": this.list.servStateCd,
          "servModule": this.list.servModule,
          "servIos": this.list.servIos,
          "servAndroid": this.list.servAndroid,
          "servStatus": this.list.servStatus,
          "servCode": this.list.servCode
         
      }
      
  };
    this.enabledService.updateService(payload).subscribe(res => {
      console.log("updated ", res);

      alert("Are you sure to update service code "+ this.list.servCode)
    });

  }
}