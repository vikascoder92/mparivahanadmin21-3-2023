import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfermationComponent } from './confermation.component';

describe('ConfermationComponent', () => {
  let component: ConfermationComponent;
  let fixture: ComponentFixture<ConfermationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConfermationComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ConfermationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
