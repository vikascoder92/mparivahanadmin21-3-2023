import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-states',
  templateUrl: './states.component.html',
  styleUrls: ['./states.component.css']
})
export class StatesComponent implements OnInit {

  constructor() { }
  IsSpinner = true
  ngOnInit(): void {
    setTimeout(() => {
    this.IsSpinner = false
    }, 2000);
  }
  

}
