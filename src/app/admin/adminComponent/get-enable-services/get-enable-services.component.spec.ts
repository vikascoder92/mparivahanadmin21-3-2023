import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GetEnableServicesComponent } from './get-enable-services.component';

describe('GetEnableServicesComponent', () => {
  let component: GetEnableServicesComponent;
  let fixture: ComponentFixture<GetEnableServicesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [GetEnableServicesComponent]
    })
      .compileComponents();

    fixture = TestBed.createComponent(GetEnableServicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
