import { Injectable } from "@angular/core";
// import { environment } from "src/environments/environment";
import { HttpService } from "./http.service";

@Injectable({
    providedIn: 'root'
})
export class EnabledService{
    baseUrl: any;    
    constructor(private httpService: HttpService){
        // this.baseUrl = environment.apiBaseUlr;
    }

    getAllEnabledServices(body:object){
        const url: string = `https://staging.parivahan.gov.in/admin-stg/api/admin/getenableservices`;
        return this.httpService.post(url,body);
    }

    updateService(body: object){
        const url: string = `https://staging.parivahan.gov.in/admin-stg/api/admin/updateEnableServices`;
        return this.httpService.post(url,body);
    }


    sarathiAllCount(body:object){
        const url: string = `https://staging.parivahan.gov.in/admin-stg/api/dashboard/get-sarathi-count`;
        return this.httpService.post(url,body);
    }
}