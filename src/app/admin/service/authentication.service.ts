import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  // baseUrl: string;
  constructor(private http: HttpClient) {
    // this.baseUrl = environment.apiBaseUlr;  
  }

  login(username: any, password: any){
    const url: string = `https://staging.parivahan.gov.in/admin-stg/api/auth/signin`;
    const payload: object = {
      username: username,
      password: password
    };
    return this.http.post(url, payload);
  }

  saveLoginData(loginData: object){
    sessionStorage.setItem("admin-login-data", JSON.stringify(loginData));
  }


  logout(): void {
    sessionStorage.removeItem('admin-login-data');
  }
  public get loggedIn(): boolean {
    return (this.getLoggedInUser() !== null);
  }

  public getLoggedInUser() {
    try {
      const loginRawData: any = sessionStorage.getItem('admin-login-data');
      return JSON.parse(loginRawData);
    } catch (error) {
      return null;
    }
  }

  public getAuthToken(){
    const loginData = this.getLoggedInUser();
    return loginData ? loginData.accessToken : null;
  }
}
