import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from  '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { LoginComponent } from './login/login.component';
import { AuthenticationService } from './admin/service/authentication.service';
import { AuthGuard } from './admin/service/auth.guard';
import { AdminModule } from './admin/admin.module';
import { CommonModule, HashLocationStrategy, LocationStrategy } from '@angular/common';
import { SignupComponent } from './signup/signup.component';
import { ErrorComponent } from './error/error.component';
import { DashComponent } from './dash/dash.component';
import { GetCountService } from './services/get-count.service';
import { ContactDetailsComponent } from './contact-details/contact-details.component';
import { HomeComponent } from './home/home.component';
import { DashheaderComponent } from './shared/dashheader/dashheader.component';
import { PrivacyPolicyComponent } from './footerLink/privacy-policy/privacy-policy.component';
import { TermsComponent } from './footerLink/terms/terms.component';
import { WebsitePolicyComponent } from './footerLink/website-policy/website-policy.component';
import { HyperlinkPolicyComponent } from './footerLink/hyperlink-policy/hyperlink-policy.component';
import { ContentPolicyComponent } from './footerLink/content-policy/content-policy.component';
import { ContingencyComponent } from './footerLink/contingency/contingency.component';
import { InterceptorInterceptor } from './interceptor.interceptor';
import { EnabledService } from './admin/service/enabled-service.service';



@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    LoginComponent,
    SignupComponent,
    ErrorComponent,
    DashComponent,
    ContactDetailsComponent,
    HomeComponent,
    DashheaderComponent,
    PrivacyPolicyComponent,
    TermsComponent,
    WebsitePolicyComponent,
    HyperlinkPolicyComponent,
    ContentPolicyComponent,
    ContingencyComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
   
    CommonModule,
    FormsModule,
    AdminModule,
    HttpClientModule,



    
  ],
  providers: [AuthenticationService,AuthGuard,GetCountService,EnabledService,
  {provide : HTTP_INTERCEPTORS , useClass : InterceptorInterceptor , multi : true} ],
  bootstrap: [AppComponent]
})
export class AppModule { }
