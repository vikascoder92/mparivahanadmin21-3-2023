import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

@Injectable({
    providedIn: 'root'
})
export class HttpService{
    constructor(private http: HttpClient){

    }

    get(url: string, options?: object){
        return this.http.get(url, options);
    }

    post(url: string, body: any, options?: object){
        return this.http.post(url, body, options);
    }
}