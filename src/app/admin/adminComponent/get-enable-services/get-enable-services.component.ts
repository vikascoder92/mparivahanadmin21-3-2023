import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GetCountService } from 'src/app/services/get-count.service';
import { EnabledService } from '../../service/enabled-service.service';

@Component({
  selector: 'app-get-enable-services',
  templateUrl: './get-enable-services.component.html',
  styleUrls: ['./get-enable-services.component.css']
})
export class GetEnableServicesComponent implements OnInit {
  lists: any;
  order: any;
  VahanLists: any;
  echallanLists: any;
  noticeLists: any;
  check = false;
  filters = {
    mparEnabledService: {
      servStateCd: "DL",
      servModule: "S4"
    }
  };
  IsSpinner = true
  constructor(private _router: Router, private router: ActivatedRoute, private enabledService: EnabledService) {

  }


  ngOnInit(): void {
    this.getenableSarathi();
    setTimeout(() => {
      this.IsSpinner = false
    }, 1000);

  }

  getenableServices(): void {
    this.enabledService.getAllEnabledServices(this.filters).subscribe((d: any) => {
      this.lists = d;
      console.log(this.lists);
    })

  }

  onEditClick(service: any) {
    this._router.navigate(['/admin/updateService', this.filters.mparEnabledService.servStateCd, this.filters.mparEnabledService.servModule, service.servId]);
  }

  onServModuleChange(servModule: string) {
    this.filters.mparEnabledService.servModule = servModule;
    this.getenableServices();
  }

  getenableSarathi(): void {

    let body = {
      "mparEnabledService": {
        "servStateCd": "DL",
        "servModule": "S4"
      }
    }
    this.enabledService.getAllEnabledServices(body).subscribe((d: any) => {
      this.lists = d;
      console.log(this.lists)

    })

  }

  getenableVahan(): void {

    let body = {
      "mparEnabledService": {
        "servStateCd": "DL",
        "servModule": "V4"
      }
    }
    this.enabledService.getAllEnabledServices(body).subscribe((d: any) => {
      this.VahanLists = d;
      console.log(this.VahanLists)

    })

  }
  getenableEchallan(): void {

    let body = {
      "mparEnabledService": {
        "servStateCd": "DL",
        "servModule": "E4"
      }
    }
    this.enabledService.getAllEnabledServices(body).subscribe((d: any) => {
      this.echallanLists = d;
      console.log(this.echallanLists)

    })

  }
  getenableNotice(): void {

    let body = {
      "mparEnabledService": {
        "servStateCd": "DL",
        "servModule": "N4"
      }
    }
    this.enabledService.getAllEnabledServices(body).subscribe((d: any) => {
      this.noticeLists = d;
      console.log(this.noticeLists)

    })

  }

  onItemChange(value: any) {
    this.order = value;
    console.log(this.order);
  }


}

