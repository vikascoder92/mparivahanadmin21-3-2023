import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminComponent } from './admin.component';
import { AuthGuard } from './service/auth.guard';
import { ContactComponent } from './adminComponent/contact/contact.component';
import { DashboardComponent } from './adminComponent/dashboard/dashboard.component';
import { ProfileComponent } from './adminComponent/profile/profile.component';
import { UserDetailsComponent } from './adminComponent/user-details/user-details.component';
import { StatesComponent } from './adminComponent/states/states.component';
import { ErrorComponent } from './adminComponent/error/error.component';
import { FormComponent } from './adminComponent/form/form.component';
import { UpdateServiceComponent } from './adminComponent/update-service/update-service.component';
import { GetEnableServicesComponent } from './adminComponent/get-enable-services/get-enable-services.component';
const routes: Routes = [
  {
    path: '', component: AdminComponent, canActivate: [AuthGuard],
    children: [

      { path: '', component: DashboardComponent },
      { path: 'dashboard', component: DashboardComponent },
      { path: 'profile', component: ProfileComponent },
      { path: 'contactDetails', component: ContactComponent },
      { path: 'userList', component: UserDetailsComponent },
      { path: 'updateService/:stateCode/:serviceMode/:serviceId', component: UpdateServiceComponent },
      { path: 'states', component: StatesComponent },
      { path: 'createSevice', component: FormComponent },
      { path: 'getenableservice', component: GetEnableServicesComponent },
      { path: '**', component: ErrorComponent },
    ]
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
