import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContingencyComponent } from './contingency.component';

describe('ContingencyComponent', () => {
  let component: ContingencyComponent;
  let fixture: ComponentFixture<ContingencyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContingencyComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ContingencyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
